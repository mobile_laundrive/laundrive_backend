var bcrypt = require("bcryptjs");
const users = [
  {
    id: 1,
    first_name: "Laundrive",
    last_name: "Admin",
    email: "admin@gmail.com",
    password: bcrypt.hashSync('test12345', 8),
    phone: "09350260003",
    verified: true,
    roleId: 1
  },
  {
    id: 2,
    first_name: "Laundrive",
    last_name: "Customer Support",
    email: "customer_support@gmail.com",
    password: bcrypt.hashSync('test12345', 8),
    phone: "09350260004",
    verified: true,
    roleId: 2
  },
  {
    id: 3,
    first_name: "Laundrive",
    last_name: "Laundry Support",
    email: "laundry_support@gmail.com",
    password: bcrypt.hashSync('test12345', 8),
    phone: "09350260005",
    verified: true,
    roleId: 3
  },
  {
    id: 4,
    first_name: "Laundrive",
    last_name: "Driver Support",
    email: "driver_support@gmail.com",
    password: bcrypt.hashSync('test12345', 8),
    phone: "09350260006",
    verified: true,
    roleId: 4
  },

  {
    id: 5,
    first_name: "John",
    last_name: "Doe",
    email: "doe@gmail.com",
    password: bcrypt.hashSync('test12345', 8),
    phone: "09350260006",
    verified: true,
    roleId: 5
  },

  {
    id: 6,
    first_name: "Jerome",
    last_name: "Lopez",
    email: "lopezjerome@gmail.com",
    password: bcrypt.hashSync('test12345', 8),
    phone: "09350260006",
    verified: true,
    roleId: 6
  },
]

module.exports = users;