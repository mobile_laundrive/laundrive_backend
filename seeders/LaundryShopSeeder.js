const laundryShops = [
  {
    id: 1,
    name: "Wash & Dry",
    address: "Cebu City",
    more_details: "more details",
  },
  {
    id: 2,
    name: "Clean Laundry Shop",
    address: "Mandaue City",
    more_details: "more details",
  },
]

module.exports = laundryShops;