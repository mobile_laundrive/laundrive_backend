const USERS = require("./seeders/UserSeeder");
const LAUNDRY_SHOPS = require("./seeders/LaundryShopSeeder");
const BOOKING_STATUSES = require("./app/constants/statuses");
const db = require("./app/models");
const demoSeeder = require("./app/demo-seeders");
const LOAD_TYPES = db.LOAD_TYPES;
const SERVICE_TYPES = db.SERVICE_TYPES;
const User = db.user;
const ROLES = db.ROLES;
const OPERATION_CITIES = db.OPERATION_CITIES;
const BookingStatus = db.bookingStatus;
const LaundryShop = db.laundryShop

db.sequelize.sync({ force: true }).then(() => {
  console.log('Drop and Resync Db');
  initial().then(() => console.log('Done...'));
});

async function initial() {
  // Baseline
  await db.role.bulkCreate(ROLES);
  await db.loadType.bulkCreate(LOAD_TYPES);
  await db.serviceType.bulkCreate(SERVICE_TYPES);
  await db.operationCity.bulkCreate(OPERATION_CITIES);
  for (const [key, value] of Object.entries(BOOKING_STATUSES)) {
    await BookingStatus.create({ name: value });
  }

  // npm run migration --demo-seed
  if (!process.env.npm_config_demo_seed) {

    // create users
    for (let i = 0; i < USERS.length; i++) {
      await User.create(USERS[i]);
    }
  
    // create users
    for (let i = 0; i < LAUNDRY_SHOPS.length; i++) {
      await LaundryShop.create(LAUNDRY_SHOPS[i]);
    }
  }

  // npm run migration --demo-seed
  if (process.env.npm_config_demo_seed) {
    demoSeeder.run(db);
  }
}