----------

# Getting started

## Installation

Clone the repository

    git clone git@bitbucket.org:mobile_laundrive/laundrive_backend.git

Switch to the repo folder

    cd laundrive_backend

Install all the dependencies

    npm install

Copy the example env file and make the required configuration changes in the .env file

    cp .env.example .env

Start the server

    npm start

Run Migration

    npm run migration

