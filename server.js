const express = require('express');
const cors = require('cors');
const vhost = require('vhost');

const envConfig = require('dotenv').config()

const app = express();

app.use(cors());

// parse requests of content-type - application/json
app.use(express.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));

// simple route
app.get("/", (req, res) => {
  res.json({ message: "Welcome to laundrive application." });
});

// routes
require('./app/routes/auth.routes')(app);
require('./app/routes/profile.routes')(app);
require('./app/routes/user.routes')(app);
require('./app/routes/operating-cities.routes')(app);
require('./app/routes/booking.routes')(app);

// laundryshop app routes
require('./app/routes/app-laundryhop/laundryshop.routes')(app);
require('./app/routes/app-laundryhop/store-settings.routes')(app);

// set port, listen for requests
const PORT = envConfig.parsed?.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});