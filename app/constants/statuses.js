const statuses = {
  waiting: 'Waiting for Driver',
  cancelled: 'Cancelled',
  onTheWay: 'Driver is on the way',
  pickup: 'Laundry Pickup',
  washing: 'Washing',
  onTheWayToDeliver: 'Driver is on the way to deliver',
  delivered: 'Laundry delivered',
}
module.exports = statuses;
