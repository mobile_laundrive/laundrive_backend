module.exports = (sequelize, Sequelize) => {
  const BookingStatus = sequelize.define("booking_status", {
    name: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: ""
    },
  },{
    underscored: true,
    timestamps: true
  });

  return BookingStatus;
};