module.exports = (sequelize, Sequelize) => {
    const CustomerAddress = sequelize.define("customer_address", {
      address_line: {
        type: Sequelize.STRING,
        allowNull: true,
        defaultValue: ""
      },
      landmark: {
        type: Sequelize.STRING,
        allowNull: true,
        defaultValue: ""
      },
    },{
      underscored: true,
      timestamps: true
    });
  
    return CustomerAddress;
  };