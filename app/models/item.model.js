module.exports = (sequelize, Sequelize) => {
  const Item = sequelize.define("item", {
    pickup_address: {
      type: Sequelize.STRING,
      allowNull: true,
    },
    delivery_address: {
      type: Sequelize.STRING,
      allowNull: true,
    },
    notes: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: ""
    },
    estimated_kg: {
      type: Sequelize.FLOAT(11),
      allowNull: true,
      defaultValue: 0.0
    },
    kilos: {
      type: Sequelize.FLOAT(11),
      allowNull: true,
      defaultValue: 0.0
    },
    load: {
      type: Sequelize.INTEGER,
      allowNull: true,
      defaultValue: 0
    },
  },{
    underscored: true,
    timestamps: true
  });

  return Item;
};