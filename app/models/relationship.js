module.exports = (db) => {
  db.role.hasMany(db.user, {
    foreignKey: {
      name: 'roleId'
    }
  });
  
  db.user.belongsTo(db.role, { as: 'role' });

  db.otp.hasOne(db.userLoginSession);
  db.userLoginSession.belongsTo(db.otp);

  db.device.belongsTo(db.user);
  db.user.hasMany(db.device);

  db.operationCity.hasOne(db.customerAddress);
  
  db.customerAddress.belongsTo(db.user, {
    onDelete: 'cascade'
  });
  db.user.hasOne(db.customerAddress);

  // db.driver.belongsTo(db.user, { as: 'user' });
  db.driverLicense.belongsTo(db.user, { as: 'user' });
  db.vehicle.belongsTo(db.user, { as: 'user' });
  
  db.booking.belongsTo(db.user, { 
    onDelete: 'cascade',
    as:'driver',
    foreignKey: 'driver_id' 
  });

  db.booking.belongsTo(db.user,{ 
    as:'user',
    foreignKey: 'user_id' 
  });

  db.booking.belongsTo(db.laundryShop, {
    as: 'laundryShop',
    foreignKey: 'laundry_shop_id'
  });

  db.booking.belongsTo(db.item, {
    as: 'items',
    foreignKey: 'item_id'
  });

  db.item.belongsTo(db.storeService, {
    as: 'store_services',
    foreignKey: {
      name: 'store_service_id'
    }
  });

  db.booking.belongsTo(db.bookingStatus, {
    foreignKey: {
      name: 'booking_statuses_id'
    }
  });
  
  db.bookingStatus.hasMany(db.booking, {
    foreignKey: {
      name: 'booking_statuses_id'
    }
  });
  
  db.user.hasMany(db.bookingHistory, {
    foreignKey: {
      name: 'user_id'
    }
  });
  db.bookingHistory.belongsTo(db.booking, {
    foreignKey: {
      name: 'booking_id',
    }
  });
  
  db.payment.belongsTo(db.booking, { as: 'booking' });
  db.modePayment.hasMany(db.payment, {
    foreignKey: {
      name: 'mode_payment_id'
    }
  });
  
  db.laundryShopUser.belongsTo(db.user, { as: 'user' });  
  db.user.hasMany(db.laundryShopUser, {
    foreignKey: 'user_id'
  });

  db.user.hasMany(db.laundryShopUser, {
    foreignKey: 'user_id'
  });

  db.laundryShop.hasMany(db.laundryShopUser, {
    foreignKey: {
      name: 'laundry_shop_id'
    }
  });
  
  db.storeSettings.belongsTo(db.laundryShop, {
    foreignKey: {
      name: 'laundry_shop_id'
    }
  });
  
  db.laundryShop.hasOne(db.storeSettings, {
    as: 'storeSettings',
    foreignKey: {
      name: 'laundry_shop_id'
    }
  });
  
  db.storeSettings.hasMany(db.storeService, {
    foreignKey: {
      name: 'store_settings_id'
    }
  });
  
  db.storeService.belongsTo(db.loadType, {
    foreignKey: {
      name: 'load_type_id'
    }
  });
  
  db.storeService.belongsTo(db.serviceType, {
    foreignKey: {
      name: 'service_type_id'
    }
  });
};