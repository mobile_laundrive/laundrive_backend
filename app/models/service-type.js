module.exports = (sequelize, Sequelize) => {
  const ServiceType = sequelize.define("service_type", {
    name: {
      type: Sequelize.STRING,
      allowNull: false
    }
  },{
    underscored: true
  });

  return ServiceType;
};