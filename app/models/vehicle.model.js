module.exports = (sequelize, Sequelize) => {
  const Vehicle = sequelize.define("vehicle", {
    brand: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: ""
    },
    year_model: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: ""
    },
    plate_number: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: ""
    },
    remarks: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: ""
    },
  },{
    underscored: true,
    timestamps: true
  });

  return Vehicle;
};