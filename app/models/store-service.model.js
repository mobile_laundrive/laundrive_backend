module.exports = (sequelize, Sequelize) => {
    const StoreService = sequelize.define("store_service", {
      load_type_id: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      service_type_id: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      // Rush Laundry
      max_daily_transactions: {
        type: Sequelize.DECIMAL,
        allowNull: true
      },
      min_kg: {
        type: Sequelize.DECIMAL,
        allowNull: true
      },
      // Per-Load
      max_kg: {
        type: Sequelize.DECIMAL,
        allowNull: true
      },
      price: {
        type: Sequelize.DECIMAL,
        allowNull: false
      },
      delivery_time: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      delivery_time_unit: {
        type: Sequelize.STRING,
        allowNull: false
      },
      is_active: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false
      },
    },{
      underscored: true
    });
  
    return StoreService;
  };