const config = require("../config/db.config.js");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(
  config.DB,
  config.USER,
  config.PASSWORD,
  {
    host: config.HOST,
    dialect: config.dialect,

    pool: {
      max: config.pool.max,
      min: config.pool.min,
      acquire: config.pool.acquire,
      idle: config.pool.idle
    }
  }
);

sequelize
  .authenticate()
  .then(() => {
    console.log('Connected to the database.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });

const db = {};

db.associations = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.user = require("../models/user.model.js")(sequelize, Sequelize);
db.role = require("../models/role.model.js")(sequelize, Sequelize);
db.serviceType = require("../models/service-type.js")(sequelize, Sequelize);
db.loadType = require("../models/load-type.model.js")(sequelize, Sequelize);
db.storeService = require("../models/store-service.model.js")(sequelize, Sequelize);
db.storeSettings = require("../models/store-settings.model.js")(sequelize, Sequelize);
db.otp = require("../models/otp.model.js")(sequelize, Sequelize);
db.booking = require("../models/booking.model.js")(sequelize, Sequelize);
db.bookingStatus = require("../models/booking_status.model.js")(sequelize, Sequelize);
db.bookingHistory = require("../models/booking_history.model.js")(sequelize, Sequelize);
db.item = require("../models/item.model.js")(sequelize, Sequelize);
db.laundryShop = require("../models/laundry_shop.model.js")(sequelize, Sequelize);
db.laundryShopUser = require("../models/laundry_shop_user.model.js")(sequelize, Sequelize);
db.payment = require("../models/payment.model.js")(sequelize, Sequelize);
db.modePayment = require("../models/mode_payment.model")(sequelize, Sequelize);
db.userLoginSession = require("./user-login-session.model")(sequelize, Sequelize);
db.device = require("./device.model")(sequelize, Sequelize);
db.customerAddress = require("./customer_address.model")(sequelize, Sequelize);
db.operationCity = require("./operation_city.model")(sequelize, Sequelize);

// db.driver = require("../models/driver.model.js")(sequelize, Sequelize); 
db.driverLicense = require("../models/driver_license.model.js")(sequelize, Sequelize);
db.vehicle = require("../models/vehicle.model.js")(sequelize, Sequelize);

db.ROLES = require('../baseline-seeds/roles.seed');
db.SERVICE_TYPES = require('../baseline-seeds/service-types.seed');
db.LOAD_TYPES = require('../baseline-seeds/load-types.seed');
db.Roles = db.ROLES;
db.OPERATION_CITIES = require("../baseline-seeds/operation-city.seed")
db.relationShip = require("../models/relationship")(db);


module.exports = db;