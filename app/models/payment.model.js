module.exports = (sequelize, Sequelize) => {
  const Payment = sequelize.define("payment", {
    amount: {
      type: Sequelize.FLOAT,
      allowNull: true,
      defaultValue: 0
    },
    description: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: ""
    },
  },{
    underscored: true,
    timestamps: true
  });

  return Payment;
};