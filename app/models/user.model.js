module.exports = (sequelize, Sequelize) => {
  const User = sequelize.define("user", {
    first_name: {
      type: Sequelize.STRING,
      allowNull: false,
      defaultValue: ""
    },
    last_name: {
      type: Sequelize.STRING,
      allowNull: false,
      defaultValue: ""
    },
    email: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: ""
    },
    password: {
      type: Sequelize.STRING,
      allowNull: false,
      defaultValue: ""
    },
    phone: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: ""
    },
    verified: {
      type: Sequelize.BOOLEAN, 
      allowNull: false, 
      defaultValue: false,
    },
    default_password: {
      type: Sequelize.BOOLEAN, 
      allowNull: false, 
      defaultValue: true,
    },
  },{
    underscored: true,
    timestamps: true
  });

  return User;
};