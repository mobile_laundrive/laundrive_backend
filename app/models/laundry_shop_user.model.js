module.exports = (sequelize, Sequelize) => {
  const LaundryShopUser = sequelize.define("laundry_shop_user", {},
  {
    underscored: true,
    timestamps: true
  });

  return LaundryShopUser;
};