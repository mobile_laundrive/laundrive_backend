module.exports = (sequelize, Sequelize) => {
  const LoadType = sequelize.define("load_type", {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false
    },
  },{
    underscored: true
  });

  return LoadType;
};