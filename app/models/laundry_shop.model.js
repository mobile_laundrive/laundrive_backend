module.exports = (sequelize, Sequelize) => {
  const LaundryShop = sequelize.define("laundry_shop", {
    name: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: ""
    },
    address: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: ""
    },
    more_details: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: ""
    },
    remarks: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: ""
    },
    is_online: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    first_setup: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: true
    }
  },{
    underscored: true,
    timestamps: true
  });

  return LaundryShop;
};