module.exports = (sequelize, Sequelize) => {
    const Device = sequelize.define("device", {
        name: {
            type: Sequelize.STRING,
            allowNull: false,
            defaultValue: ""
        },
        expiry: {
            type: Sequelize.DATE,
            allowNull: true,
        }
    },{
        underscored: true,
        timestamps: true
    });

    return Device;
};