module.exports = (sequelize, Sequelize) => {
  const DriverLicense = sequelize.define("driver_lincense", {
    name: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: ""
    },
    expiry: {
      type: Sequelize.DATE,
      allowNull: true,
    },
    proof: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: ""
    },
    remarks: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: ""
    },
  },{
    underscored: true,
    timestamps: true
  });

  return DriverLicense;
};