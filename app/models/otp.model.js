module.exports = (sequelize, Sequelize) => {
    const Otp = sequelize.define("otp_code", {
      code: {
        type: Sequelize.STRING,
        allowNull: false,
        defaultValue: ""
      },
      expired: {
        type: Sequelize.BOOLEAN, 
        allowNull: false,
        defaultValue: false,
      },
      expiry: {
        type: Sequelize.DATE,
        allowNull: true,
      }
    }, {
      underscored: true,
      timestamps: true
    });
  
    return Otp;
};