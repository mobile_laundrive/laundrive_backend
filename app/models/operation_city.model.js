module.exports = (sequelize, Sequelize) => {
    const OperationCity = sequelize.define("operation_city", {
        name: {
            type: Sequelize.STRING,
            allowNull: true,
            defaultValue: null
        },
    }, {
        underscored: true,
        timestamps: true
    });

    return OperationCity;
};