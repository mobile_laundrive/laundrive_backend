module.exports = (sequelize, Sequelize) => {
  const ModePayment = sequelize.define("mode_payment", {
    name: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: ""
    }
  },{
    underscored: true,
    timestamps: true
  });

  return ModePayment;
};