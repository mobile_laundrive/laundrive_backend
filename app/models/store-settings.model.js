module.exports = (sequelize, Sequelize) => {
  const Store = sequelize.define("store_settings", {
    logo: {
      type: Sequelize.STRING,
      allowNull: true,
    },
    background: {
      type: Sequelize.STRING,
      allowNull: true,
    },
    open_hours: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    closing_hours: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    accept_rush: {
      type: Sequelize.BOOLEAN,
      allowNull: false
    }
  },{
    underscored: true
  });

  return Store;
};