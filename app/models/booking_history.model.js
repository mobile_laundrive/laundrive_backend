module.exports = (sequelize, Sequelize) => {
  const BookingHistory = sequelize.define("booking_history", {
    remarks: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: ""
    },
  },{
    underscored: true,
    timestamps: true
  });

  return BookingHistory;
};