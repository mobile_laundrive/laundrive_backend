module.exports = (sequelize, Sequelize) => {
  const Booking = sequelize.define("booking", {
    amount: {
      type: Sequelize.FLOAT(11),
      allowNull: true,
      defaultValue: 0.0
    },
  },
  {
    underscored: true,
    timestamps: true
  });

  return Booking;
};