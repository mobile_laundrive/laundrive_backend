module.exports = (sequelize, Sequelize) => {
    const UserLoginSession = sequelize.define("user_login_session", {
      email: {
        type: Sequelize.STRING,
        allowNull: true,
        defaultValue: ""
      },
      phone: {
        type: Sequelize.STRING,
        allowNull: true,
        defaultValue: ""
      },
      verified: {
        type: Sequelize.BOOLEAN, 
        allowNull: false, 
        defaultValue: false,
      },
      expiry: {
        type: Sequelize.DATE,
        allowNull: true,
      }
    },{
      underscored: true,
      timestamps: true
    });
  
    return UserLoginSession;
  };