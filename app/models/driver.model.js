module.exports = (sequelize, Sequelize) => {
  const Driver = sequelize.define("driver", {
    remarks: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: ""
    },
  },{
    underscored: true,
    timestamps: true
  });

  return Driver;
};