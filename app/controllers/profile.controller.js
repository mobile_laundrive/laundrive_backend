const { raw } = require("body-parser");
const db = require("../models");
const User = db.user;
const CustomerAddress = db.customerAddress;
const OperationCity = db.operationCity;
const Role = db.role;

exports.createOrUpdate = (req, res) => {

};

exports.create = async (req, res) => {

    let rawUser = req.body.user;
    let rawUserAddress = req.body.addressDetails;

    if (null == rawUser || null == rawUserAddress) {
        res.send({data:{message: "Invalid request", error:true}});
        return
    }

    let currentProfile = await User.findOne({
        where: {email: rawUser.email}
    });

    if (currentProfile) {
        res.send({
            data: {message: "User already exists", error:true}
        })
        return
    }

    let params = {
        first_name: rawUser.first_name,
        last_name: rawUser.last_name,
        email: rawUser.email,
        phone: rawUser.phone,
        verified: rawUser.verified,
        roleId: rawUser.roleId
    };

    let createdUser = await User.create(params)

    let customerAddressParams = {
        address_line: rawUserAddress.address_line,
        landmark: rawUserAddress.landmark,
    };

    let city = await OperationCity.findOne({
        where: {
            name: rawUserAddress.cityName
        }
    })

    let createdAddress = await CustomerAddress.create(customerAddressParams);
    await createdAddress.update({
        userId: createdUser.id,
        operationCityId: city.id
    });

    createdAddress.cityName = city.name;

    if (null != createdUser && null != createdAddress) {
        res.send({
            data: {
                customerProfile: {
                    user: createdUser,
                    addressDetails: createdAddress
                }
            }
        })
    } else {
        res.send({
            data: {message: "Failed to create user", error:true}
        })
    }

};

exports.update = (req, res) => {

    let whereArgs = {};

    let email = req.body.email;
    if (email) {
        whereArgs.email = email;
    }

    let phone = req.body.phone;
    if (phone) {
        whereArgs.phone = phone;
    }

    User
        .findOne({where: whereArgs})
        .then(async result => {
            var updatedUser = await result.update(req.body.user);
            res.send({
                data: {
                    user: updatedUser
                }
            });
        })
        .catch(error => res.send({data:{message: error.message, error:true}}));
};

exports.delete = (req, res) => {

}