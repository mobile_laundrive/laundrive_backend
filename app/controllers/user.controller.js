// services
const userService = require("../services/user.services");

exports.allAccess = (req, res) => {
  res.status(200).send("Public Content.");
};

exports.customerBoard = async (req, res) => {
  try { 
    let { page, size, search } = req.query;
    const customers = await userService.getCustomers(page, size, search);

    return res.status(200).send({
      status: 200,
      data: customers
    });
  } catch (err) {
    return res.status(500).send({data:{ message: "Something went wrong!" }});
  }
};

exports.adminBoard = (req, res) => {
  res.status(200).send("Admin Content.");
};

exports.laundryOwnerBoard = (req, res) => {
  res.status(200).send("Laundry Owner Content.");
};


exports.driverBoard = (req, res) => {
  res.status(200).send("Driver Content.");
};

exports.userBoard = (req, res) => {
  res.status(200).send("Customer Content.");
};