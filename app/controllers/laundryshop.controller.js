const laundryShopByUserMapper = require("../mapper/laundry-shops-by-user.mapper");
const db = require("../models");
const laundryShopService = require("../services/laundry-shop.services");
const bookingService = require('../services/booking.service');
const statuses = require("../constants/statuses");

exports.getAll = (_, res) => {
  laundryShopService.getLaundryShops()
    .then(existingShops => {
      res.status(200).send({
        data: {shops: existingShops}
      })
    })
}

exports.getAllByUser = (req, res) => {
  laundryShopService.getAllByUser(req.userId, res)
    .then(laundryshops => {
      res.status(200).send({
        status: 200,
        data: laundryShopByUserMapper(laundryshops)
      });
    })
    .catch(() => {
      res.status(500).send({ data: { message: "Something went wrong!" } });
    });
};

exports.getById = (req, res) => {
  db.laundryshop.findOne({
    where: {
      id: req.params.laundryshopId
    },
    include: [{
      model: db.storeSettings,
      include: [
        {
          model: db.storeService,
          include: [
            {
              model: db.loadType,
              required: true
            },
            {
              model: db.serviceType,
              required: true
            }
          ]
        }
      ]
    }]
  }).then(async laundryshop => {
    res.status(200).send({
      status: 200,
      data: laundryshop
    });
  })
    .catch(() => {
      res.status(500).send({ data: { message: "Something went wrong!" } });
    });
};

exports.updateStatus = (req, res) => {
  const shopId = req.params.laundryshopId;
  laundryShopService.updateStatus(shopId, req.body.isOnline)
    .then(() => laundryShopService.getById(shopId))
    .then((laundryshop) => {
      res.status(200).send({
        status: 200,
        data: laundryShopByUserMapper(laundryshop)
      });
    })
    .catch(() => {
      res.status(500).send({ data: { message: "Something went wrong!" } });
    });
}

exports.getBookingsSearch = (req, res) => {
  bookingService.getAllByShopIdWithSearchTerm(req.params.laundryshopId, req.query.status_id, req.query.search)
    .then((bookings) => {
      res.status(200).send({
        status: 200,
        data: bookings
      });
    });
}

exports.getBooking = (req, res) => {
  bookingService.getBooking(req.params.bookingId)
    .then((bookingDetails) => {
      res.status(200).send({
        status: 200,
        data: bookingDetails
      });
    });
}

exports.getBookingsSearchInQueue = (req, res) => {
  const statusValues = Object.values(statuses);
  
  bookingService.getAllByShopIdWithSearchTerm(
    req.params.laundryshopId,
    [
      statusValues.indexOf(statuses.waiting) + 1,
      statusValues.indexOf(statuses.onTheWay) + 1,
      statusValues.indexOf(statuses.pickup) + 1,
    ],
    req.query.search
  ).then((bookings) => {
    res.status(200).send({
      status: 200,
      data: bookings
    });
  });
}

exports.getBookingsSearchInWashing = (req, res) => {

  const statusValues = Object.values(statuses);
  bookingService.getAllByShopIdWithSearchTerm(
    req.params.laundryshopId,
    [
      statusValues.indexOf(statuses.washing) + 1
    ],
    req.query.search
  ).then((bookings) => {
    res.status(200).send({
      status: 200,
      data: bookings
    });
  });
}

exports.getBookingsSearchInReadyToPickup = (req, res) => {

  const statusValues = Object.values(statuses);
  bookingService.getAllByShopIdWithSearchTerm(
    req.params.laundryshopId,
    [
      statusValues.indexOf(statuses.onTheWayToDeliver) + 1,
    ],
    req.query.search
  ).then((bookings) => {
    res.status(200).send({
      status: 200,
      data: bookings
    });
  });
}


exports.getBookingsSearchInCompleted = (req, res) => {

  const statusValues = Object.values(statuses);
  bookingService.getAllByShopIdWithSearchTerm(
    req.params.laundryshopId,
    [statusValues.indexOf(statuses.delivered) + 1],
    req.query.search
  ).then((bookings) => {
    res.status(200).send({
      status: 200,
      data: bookings
    });
  });
}

