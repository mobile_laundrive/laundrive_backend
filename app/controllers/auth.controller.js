const db = require("../models");
const config = require("../config/auth.config");
const User = db.user;
const Role = db.role;
const Otp = db.otp;
const UserLoginSession = db.userLoginSession
const Device = db.device;

const Op = db.Sequelize.Op;
const TOKEN_EXPIRY = 86400;

var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");
const { generateOTP } = require("../utils/otp.util");
const nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
var handlebars = require('handlebars');
var fs = require('fs');
const { dirname } = require ( 'path' );

const { DATE, where } = require("sequelize");
const { device, user, userLoginSession } = require("../models");
const { create } = require("domain");
const e = require("cors");

exports.authenticateDevice = async (req, res) => {
  
  let deviceName = req.body.deviceName;
  let whereArgs = { name: deviceName };

  if (!deviceName) {
    res.status(401).send({
      data: {
        error: true,
        message: "UnAuthorized"
      }
    })
    return
  }

  var currentDevice = await Device.findOne(whereArgs);

  if (!currentDevice) {
    currentDevice = await Device.create({
      name: deviceName,
      expiry: new Date(Date.now() + (24 * 60 * 60 * 1000))
    });
  }

  var token = jwt.sign({ id: device.id }, config.secret, {
    expiresIn: TOKEN_EXPIRY
  });

  res.send({data: {key: token}});
};

exports.signup = async (req, res) => {

  let createArgs = {
    expiry: new Date(Date.now() + (2 * 60 * 1000))
  };

  let whereArgs = {};

  let email = req.body.email;
  if (email) {
    createArgs.email = email;
    whereArgs.email = email;
  }

  let phone = req.body.phone;
  if (phone) {
    createArgs.phone = phone;
    whereArgs.phone = phone;
  }

  let otpCode = await Otp.create(req.body.otp);
  
  let otpId = otpCode.id;
  let code = otpCode.code;

  createArgs.otpCodeId = otpId;

  UserLoginSession
    .findOne({where: whereArgs})
    .then(async existingSession => {

      if (!existingSession) {
        await UserLoginSession
          .create(createArgs)
          .then(created => res.send({data: {session: created, code: code}}))
          .catch(error => res.send({message: error.message}));
        return;
      }

      await Otp.destroy({
        where: { id: existingSession.otpCodeId }
      });

      let response = {
        session: existingSession,
        code: null
      };

      await existingSession.update({otpCodeId: otpId});
      response.code = code;

      res.send({data: response});
    });
};

exports.signin = (req, res) => {
  User.findOne({
    where: {
      email: req.body.email
    }
  },
  { include: Role })
    .then( async user => {

      if (!user) {
        return res.status(404).send({data: { message: "User Not found." }});
      }

      var passwordIsValid = bcrypt.compareSync(
        req.body.password,
        user.password
      );

      if (!passwordIsValid) {
        return res.status(401).send({
          data:{
            accessToken: null,
            message: "Invalid Password!"
        }});
      }

      if (user.verified === false) {
        return res.status(401).send({data:{
          accessToken: null,
          message: "User is not verified!"
        }});
      }

      // retrieve user role
      const role = await user.getRole()

      var token = jwt.sign({ id: user.id }, config.secret, {
        expiresIn: TOKEN_EXPIRY // 24 hours
      });

      res.status(200).send({
        status: 200,
        data:{
          id: user.id,
          first_name: user.first_name,
          last_name: user.last_name,
          email: user.email,
          role: role,
          phone: user.phone,
          otp: user.otp,
          accessToken: token,
          defaultPassword: user.default_password,
          expiresIn: process.env.TOKEN_EXPIRY
        }
      });
    })
    .catch(err => {
      res.status(500).send({data:{ message: err.message }});
    });
};

exports.verifyOtp = (req, res) => {
  const { password } = req.body; 

  Otp.findOne({
    where: {
      code: req.body.code,
      expired: false
    },
    include: UserLoginSession
  }).then(async otp => {

    if (!otp) {
      res.send({data: {message: "Sorry, We couldn't find that code.", error: true}});
      return;
    }

    // If OTP code is expired
    if(otp.expiry.getDate() > Date.now()) {
      res.send({data: {message: "OTP code is expired.", error: true}});
      return;
    }

    let loginSession = otp.user_login_session;

    // If user is verified
    if (!loginSession.verified) {
      await loginSession.update({verified: true, otpCodeId: null});
    }
    
    await otp.destroy();

    // Look for user
    var whereArgs = {};

    let email = loginSession.email;
    let phone = loginSession.phone;

    if (email) {
        whereArgs.email = email;
    }

    if (phone) {
        whereArgs.phone = phone;
    }

    User
      .findOne({where: whereArgs})
      .then(existingUser => res.send(null != existingUser && undefined != existingUser ? 
          {data: {user: existingUser}} : 
          {data: {session: loginSession, message: "Yey!, you're verified."}}))
      .catch(error => res.send({data:{message: error.message, error:true}}));
      
  }).catch(error => res.send({data:{message: error.message, error:true}}));

};

exports.updateProfile = (req, res) => {
  User.findOne({
    where: {
      id: req.body.user.id
    }
  }).then(async result => {
    result.update(req.body.user);
    res.status(200).send({user:result});
  }).catch(error => res.send({data:{message: error.message, error:true}}));
};

exports.profile = (req, res) => {
  
  User.findOne({
    where: {
      id: req.params.id
    }
  },
  { include: Role })
    .then( async user => {
      // retrieve user role
      const role = await user.getRole()

      res.status(200).send({
        status: 200,
        data:{
          id: user.id,
          first_name: user.first_name,
          last_name: user.last_name,
          email: user.email,
          role: role,
          phone: user.phone,
          otp: user.otp,
        }
      });
    })
    .catch(err => {
      res.status(500).send({data:{ message: err.message }});
    });
};

exports.sendEmailOtp = (req, res) => {
  try {
    const { email } = req.body; 

    User.findOne({
      where: {
        email,
      }
    }).then(async user => {
      if (!user) {
        return res.status(404).send({data:{ message: "Email not found." }});
      }

      // Create nodemailer transporter
      const EMAIL_ADDRESS = 'noreply@api.laundrive.xyz';
      const {subject_mail, message} = require('../templates/email/verification');
      const otp = generateOTP(6);


      var readHTMLFile = function(path, callback) {
        fs.readFile(path, {encoding: 'utf-8'}, function (err, html) {
            if (err) {
               callback(err); 
               throw err;
                
            }
            else {
                callback(null, html);
            }
        });
      };

      smtpTransport = nodemailer.createTransport(smtpTransport({
        host: 'smtp.mailtrap.io',
        port: 587,
        secureConnection: false,
        auth: {
          user: '16cc68d1695d1a',
          pass: 'ecdc9c75d30aec'
        },
        tls: {
          ciphers:'SSLv3'
        }
      }));

      readHTMLFile('app/templates/email/forget.html', function(err, html) {
        var template = handlebars.compile(html);
        var replacements = {
          otp,
          company_name: "Laundrive Corporation"
        };
        var htmlToSend = template(replacements);
        var mailOptions = {
            from: `"Laundrive Corporation"<${EMAIL_ADDRESS}>`,
            to: `${email}`,
            subject: 'OTP Code For Reset Password',
            html : htmlToSend,
         };
        smtpTransport.sendMail(mailOptions, function (error, response) {
          if (error) {
              console.log(error);
              callback(error);
          } else {
            const currentDate = new Date();
            const mins = 5; //add minutes to date
            //create otp
            Otp.create({
              code: otp,  
              expiry: new Date(currentDate.getTime() + mins * 60000),    
              user
            })
            .then((otp) => {
              user.otpCodeId = otp.id;
              // update user
              user.save();
            })
    
            return res.send({
              data:{
                message: "email sent!"
            }});
          }
        });
      });
    })

  }  catch(err){
    const response={"Status":"Failure","Details": err.message}
    return res.status(400).send(response)
  }
};

exports.firstResetPassword = (req, res) => {
  const { password, confirmPassword } = req.body; 
  
  User.findOne({
      where: {
        id: req.userId,
      }
    }).then( user => {
      if (!user || user.length == 0 && !user.default_password) {
        return res.status(403).send({data:{ message: "Unauthorized user" }});
      }

      if (confirmPassword !== password) {
        return res.status(500).send({data:{ message: "Password and confirm password does not match!" }});
      }

      user.password = bcrypt.hashSync(password, 8);
      user.verified = true;
      user.default_password = false;
      user.save();

      return res.status(200).send({data:{
        message: 'Your password has been successfully reset',
      }});
    }).catch(err => {
      res.status(500).send({data:{ message: err.message }});
  });
};

exports.refreshLogin = (req, res) => {
  User.findOne({
      where: {
        id: req.userId,
      }
    }, {
      include: {
        model: Role,
        required: true
      }
    }).then( async (user) => {
      // retrieve user role
      const role = await user.getRole();

      var token = jwt.sign({ id: user.id }, config.secret, {
        expiresIn: TOKEN_EXPIRY // 24 hours
      });

      res.status(200).send({
        status: 200,
        data:{
          id: user.id,
          first_name: user.first_name,
          last_name: user.last_name,
          email: user.email,
          role: role,
          phone: user.phone,
          otp: user.otp,
          accessToken: token,
          defaultPassword: user.default_password,
          expiresIn: process.env.TOKEN_EXPIRY
        }
      });
    }).catch(err => {
      console.log(err);
      res.status(403).send({data:{ message: 'Unauthorize user' }});
  });
};

const resetPassword = (req, res, otp_code) => {
  const { email, otp, password, confirmPassword } = req.body; 
  User.findOne({
      where: {
        email,
        otpCodeId: otp_code.id
      }
    })
      .then( user => {
        if (!user || user.length == 0) {
          return res.status(404).send({data:{ message: "Email address is invalid" }});
        }

        if (confirmPassword !== password) {
          return res.status(500).send({data:{ message: "Password and confirm password does not match!" }});
        }

        user.password = bcrypt.hashSync(password, 8);
        user.verified = true;
        user.save();

        Otp.destroy({
          where: {code: otp}
        });

        return res.status(200).send({data:{
          message: 'Your password has been successfully reset',
        }});
      })
      .catch(err => {
        res.status(500).send({data:{ message: err.message }});
      });
}