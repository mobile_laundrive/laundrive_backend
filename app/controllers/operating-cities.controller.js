const operationCitiesService = require("../services/operation-cities.service");

exports.getAll = (_, res) => {
    operationCitiesService
        .getOperationCities()
        .then(operationCities => {
            res.status(200).send({
                data: {cities: operationCities}
            });
        });
};