const db = require("../models");

exports.storeService = (req, res) => {
  db.laundryshop.create({
    load_type_id: req.body.load_type_id,
    service_type_id: req.body.service_type_id,
    // Rush Laundry
    max_daily_transactions: req.body.max_daily_transactions,
    min_kg: req.body.min_kg,
    // Per-Load
    max_kg: req.body.max_kg,
    price: req.body.price,
    delivery_time: req.body.delivery_time,
    delivery_time_unit: req.body.delivery_time_unit,
    store_settings_id: req.body.store_settings_id,
    created_at: new Date(),
    updated_at: new Date()
  }).then(async storeSettings => {
    res.status(200).send({
      status: 200,
      data: {
        id: storeSettings.id
      }
    });
  });
};
