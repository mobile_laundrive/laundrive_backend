const { storeService, laundryShop } = require("../models");
const db = require("../models");
const shopService = require("../services/shop-service.service");
const storeSettingsService = require("../services/store-settings.service");

exports.create = (req, res) => {
  db.storeSettings.create({
    logo: req.body.logo,
    background: req.body.background,
    open_hours: req.body.open_hours,
    closing_hours: req.body.closing_hours,
    accept_rush: req.body.accept_rush,
    laundryshop_id: req.body.laundryshop_id,
    created_at: new Date(),
    updated_at: new Date()
  }).then(async storeSettings => {
    res.status(200).send({
      status: 200,
      data: {
        id: storeSettings.id
      }
    });
  });
};

exports.update = async (req, res) => {
  req.body.perKilo.rush.min_kg = req.body.perKilo.regular.min_kg;
  req.body.perLoad.rush.max_kg = req.body.perLoad.regular.max_kg;

  const kiloRegular = shopService.update(req.body.perKilo.regular.id, req.body.perKilo.regular);
  const kiloRush = shopService.update(req.body.perKilo.rush.id, req.body.perKilo.rush);
  const perLoadRegular = shopService.update(req.body.perLoad.regular.id, req.body.perLoad.regular);
  const perLoadRush = shopService.update(req.body.perLoad.rush.id, req.body.perLoad.rush);
  
  const storeSettings = storeSettingsService.update(req.params.storeSettingsId, {
    open_hours: req.body.open_hours,
    closing_hours: req.body.closing_hours,
    updated_at: new Date(),
  });

  const lShop = await laundryShop.findOne({
    where: {
      id: req.body.laundryshop_id,
    }
  });

  const statements = [
    kiloRegular,
    kiloRush,
    perLoadRegular,
    perLoadRush,
    storeSettings
  ];

  if (lShop) {
    const shop = laundryShop.update({first_setup: false, is_online: true}, {
      where: {
        id: req.body.laundryshop_id,
      }
    });
    
    statements.push(shop);
  }
  
  Promise.all(statements).then(data => {
    res.status(200).send({
      status: 200,
      data: {
        message: 'Successfully updated!'
      }
    });
  }).catch(err => {
    res.status(500).send({
      data:{ message: "Something went wrong!" }
    })
  });
};

