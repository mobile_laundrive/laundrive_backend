// services
const userService = require("../services/user.services");
const laundryShopService = require("../services/laundry-shop.services");
const bookingService = require("../services/booking.service");

exports.create = async (req, res) => {
  const {userId, driverId, laundryShopId, ...items} = req.body;

  try {
    const user = await userService.getUser(userId, res);
    const driver = await userService.getDriver(driverId, res);
    const laundryShop = await laundryShopService.getLaundryShop(laundryShopId, res);

    // booking params
    const params = {
      user_id: parseInt(user?.id),
      driver_id: parseInt(driver?.id),
      laundry_shop_id: parseInt(laundryShop?.id),
      ...items,
    };

    // create booking
    const booking = await bookingService.createBooking(params, res);
    return res.status(200).send({
      status: 200,
      data:{
        booking
      }
    });

  } catch (err) {
    return res.status(500).send({data:{ message: "Something went wrong!" }});
  }
};


exports.update = async (req, res) => {
  const {id, status, ...items} = req.body;

  try {

    // booking params
    const params = {
      id,
      status,
      ...items,
    };

    // update booking
    const booking = await bookingService.updateBooking(params, res);
    
    // return 404
    if (booking == null) {
      return res.status(404).send({
        message: "Booking not found!"
      });
    }

    return res.status(200).send({
      status: 200,
      data:{
        booking
      }
    });

  } catch (err) {
    return res.status(500).send({data:{ message: "Something went wrong!" }});
  }
}

exports.updateStatus = async (req, res) => {
  try { 
    await bookingService.updateStatus(+req.params.bookingId, +req.body.status_id);

    return res.status(200).send({
      status: 200,
      data: null
    });
  } catch (err) {
    return res.status(500).send({data:{ message: "Something went wrong!" }});
  }
}
