const users = require("./users.seeders");
const laundryshops = require("./laundryshops.seeder");
const laundryshopsUsers = require("./laundryshops-users.seeders");
const storeSettingsSeeders = require("./store-settings.seeders");
const storeServiceSeeders = require("./store-service.seeders ");
const bookingSeeder = require("./booking.seeder");
const itemsSeeder = require("./item.seeder");

module.exports = {
  async run (db) {
    await users.seed(db);
    await laundryshops.seed(db);
    await laundryshopsUsers.seed(db);
    await storeSettingsSeeders.seed(db);
    await storeServiceSeeders.seed(db);
    await bookingSeeder.seed(db);
    await itemsSeeder.seed(db);
  }
};
