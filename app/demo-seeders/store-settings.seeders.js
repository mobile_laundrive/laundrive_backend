module.exports = {
  async seed (db) {
    const shop = (await db.laundryShop.findOne({
      where: {
        name: 'The laundry shop - demo'
      }
    })).dataValues;

    return await db.storeSettings.bulkCreate([{
      id: 1,
      logo: null,
      background: null,
      open_hours: '8:30',
      closing_hours: '17:50',
      accept_rush: false,
      laundry_shop_id: shop.id,
      created_at: new Date(),
      updated_at: new Date()
    }]);
  }
};
