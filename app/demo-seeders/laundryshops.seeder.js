module.exports = {
  async seed (db) {
    return await db.laundryShop.bulkCreate([{
      id: 1,
      name: 'The laundry shop - demo',
      address: 'J-center mall, Mandaue city, cebu',
      more_details: "Wash, Dry, and Fold",
      remarks: null,
      created_at: new Date(),
      updated_at: new Date()
    }]);
  }
};
