module.exports = {
  async seed (db) {
    return await db.booking.bulkCreate([{
      id: 1,
      amount: 100,
      driver_id: 2,
      user_id: 3,
      laundry_shop_id: 1,
      booking_statuses_id: 1,
      created_at: new Date(),
      updated_at: new Date()
    },
    {
        id: 2,
        amount: 100,
        driver_id: 2,
        user_id: 3,
        laundry_shop_id: 1,
        booking_statuses_id: 1,
        created_at: new Date(),
        updated_at: new Date()
    }]);
  }
};
