module.exports = {
  async seed (db) {
    return await db.item.bulkCreate([{
      id: 1,
      pickup_address: 'A.S Fortuna, Mandaue City',
      delivery_address: 'Basak, Mandaue City',
      notes: 'Short note.',
      booking_id: 1,
      store_service_id: 1
    },
    {
      id: 2,
      pickup_address: 'Labogon, Mandaue City',
      delivery_address: 'Mabolo, Cebu City',
      notes: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      booking_id: 2,
      store_service_id: 2
    }]);
  }
};
