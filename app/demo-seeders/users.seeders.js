const bcrypt = require("bcryptjs");
const Roles = require('../baseline-seeds/roles.seed.json');

module.exports = {
  async seed (db) {
    return await db.user.bulkCreate([{
      id: 1,
      first_name: 'shop',
      last_name: 'operator',
      email: 'shop-operator@laundryshop.com',
      password: bcrypt.hashSync('password', 8),
      phone: '09123456781',
      otp: null,
      verified: true,
      roleId: Roles.find((value) => value.name === 'laundryshop_operator').id,
      is_verified: 1,
      created_at: new Date(),
      updated_at: new Date()
    },
    {
      id: 2,
      first_name: 'john',
      last_name: 'doe',
      email: 'johndoe@driver.com',
      password: bcrypt.hashSync('password', 8),
      phone: '09123456781',
      otp: null,
      verified: true,
      roleId: Roles.find((value) => value.name === 'driver').id,
      is_verified: 1,
      created_at: new Date(),
      updated_at: new Date()
    },
    {
      id: 3,
      first_name: 'john',
      last_name: 'doe',
      email: 'johndoe@customer.com',
      password: bcrypt.hashSync('password', 8),
      phone: '09123456781',
      otp: null,
      verified: true,
      roleId: Roles.find((value) => value.name === 'customer').id,
      is_verified: 1,
      created_at: new Date(),
      updated_at: new Date()
    },{
      id: 4,
      first_name: 'john',
      last_name: 'doe',
      email: 'admin@gmail.com',
      password: bcrypt.hashSync('password', 8),
      phone: '09123456781',
      otp: null,
      verified: true,
      roleId: Roles.find((value) => value.name === 'admin').id,
      is_verified: 1,
      created_at: new Date(),
      updated_at: new Date()
    }],
    );
  }
};
