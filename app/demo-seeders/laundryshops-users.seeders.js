module.exports = {
  async seed (db) {
    const shop = (await db.laundryShop.findOne({
      where: {
        name: 'The laundry shop - demo'
      }
    })).dataValues;

    const user = (await db.user.findOne({
      where: {
        email: 'shop-operator@laundryshop.com'
      }
    })).dataValues;

    return await db.laundryShopUser.bulkCreate([{
      id: 1,
      user_id: user.id,
      laundry_shop_id: shop.id,
    }]);
  }
};
