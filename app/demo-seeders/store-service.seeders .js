module.exports = {
  async seed (db) {
    const settings = (await db.storeSettings.findOne(
    { include: 
      {
        model: db.laundryShop,
        required: true,
        where: {
          name: 'The laundry shop - demo'
        }
      }
    })).dataValues;

    return await db.storeService.bulkCreate([
      {
        id: 1,
        load_type_id: db.LOAD_TYPES[0].id,
        service_type_id: db.SERVICE_TYPES[0].id,
        // Rush Laundry
        max_daily_transactions: 5,
        min_kg: 5,
        // Per-Load
        max_kg: null,
        price: 50,
        delivery_time: 3,
        delivery_time_unit: 'DAY',
        store_settings_id: settings.id,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        id: 2,
        load_type_id: db.LOAD_TYPES[0].id,
        service_type_id: db.SERVICE_TYPES[1].id,
        // Rush Laundry
        max_daily_transactions: 5,
        min_kg: 5,
        // Per-Load
        max_kg: null,
        price: 50,
        delivery_time: 4,
        delivery_time_unit: 'HOUR',
        store_settings_id: settings.id,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        id: 3,
        load_type_id: db.LOAD_TYPES[1].id,
        service_type_id: db.SERVICE_TYPES[0].id,
        // Rush Laundry
        max_daily_transactions: 5,
        min_kg: 5,
        // Per-Load
        max_kg: null,
        price: 50,
        delivery_time: 3,
        delivery_time_unit: 'DAY',
        store_settings_id: settings.id,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        id: 4,
        load_type_id: db.LOAD_TYPES[1].id,
        service_type_id: db.SERVICE_TYPES[1].id,
        // Rush Laundry
        max_daily_transactions: 5,
        min_kg: 5,
        // Per-Load
        max_kg: null,
        price: 50,
        delivery_time: 4,
        delivery_time_unit: 'HOUR',
        store_settings_id: settings.id,
        created_at: new Date(),
        updated_at: new Date()
      }
    ]);
  }
};
