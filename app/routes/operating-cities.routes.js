const { authJwt } = require("../middleware");
const controller = require("../controllers/operating-cities.controller");

module.exports = function(app) {

  app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
    next();
  });
  
  app.get("/api/operation/cities", [authJwt.verifyDeviceToken], controller.getAll);
};