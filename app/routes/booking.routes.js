const { authJwt } = require("../middleware");
const controller = require("../controllers/booking.controller");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
    next();
  });

  app.post("/api/booking/create", 
    [authJwt.verifyToken],
    controller.create
  );

  app.put("/api/booking/update", 
    [authJwt.verifyToken],
    controller.update
  );
};