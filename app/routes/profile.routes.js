const { authJwt, verification, registration, otpModule } = require("../middleware");
const controller = require("../controllers/profile.controller");

module.exports = function(app) {

  app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
    next();
  });

  app.post("/api/profile/create", [authJwt.verifyDeviceToken], controller.create);
  app.post("/api/profile/update", [authJwt.verifyDeviceToken], controller.update);
};