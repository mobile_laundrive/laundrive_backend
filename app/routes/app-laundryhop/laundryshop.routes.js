const { authJwt, booking } = require("../../middleware");
const controller = require("../../controllers/laundryshop.controller");
const bookingController = require("../../controllers/booking.controller");
const laundryshop = require("../../middleware/laundryshop");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
    next();
  });

  // Customer app-exposed
  app.get('/api/laundryshops', 
    [
      authJwt.verifyDeviceToken,
    ], 
    controller.getAll
  );
  
  app.get(
    "/laundryshop/api/shop/all",
    [
      authJwt.verifyToken,
      authJwt.isLaundryShopOperator
    ],
    controller.getAllByUser
  );

  app.get('/laundryshop/api/shop/:laundryshopId', 
    [
      authJwt.verifyToken,
      authJwt.isLaundryShopOperator,
      laundryshop.canOperate
    ],
    controller.getById
  );

  app.put('/laundryshop/api/shop/:laundryshopId/status', 
    [
      authJwt.verifyToken,
      authJwt.isLaundryShopOperator,
      laundryshop.canOperate
    ],
    controller.updateStatus
  );

  app.get('/laundryshop/api/shop/:laundryshopId/bookings/queue', 
    [
      authJwt.verifyToken,
      authJwt.isLaundryShopOperator,
      laundryshop.canOperate
    ],
    controller.getBookingsSearchInQueue
  );

  app.get('/laundryshop/api/shop/:laundryshopId/bookings/washing', 
    [
      authJwt.verifyToken,
      authJwt.isLaundryShopOperator,
      laundryshop.canOperate
    ],
    controller.getBookingsSearchInWashing
  );

  app.get('/laundryshop/api/shop/:laundryshopId/bookings/ready-to-pickup', 
    [
      authJwt.verifyToken,
      authJwt.isLaundryShopOperator,
      laundryshop.canOperate
    ],
    controller.getBookingsSearchInReadyToPickup
  );

  app.get('/laundryshop/api/shop/:laundryshopId/bookings/completed', 
    [
      authJwt.verifyToken,
      authJwt.isLaundryShopOperator,
      laundryshop.canOperate
    ],
    controller.getBookingsSearchInCompleted
  );

  app.put('/laundryshop/api/shop/:laundryshopId/bookings/:bookingId/status', 
    [
      authJwt.verifyToken,
      authJwt.isLaundryShopOperator,
      booking.isBookedToThisShop,
      laundryshop.canOperate
    ],
    bookingController.updateStatus
  );

  app.get('/laundryshop/api/shop/:laundryshopId/bookings/:bookingId', 
    [
      authJwt.verifyToken,
      authJwt.isLaundryShopOperator,
      laundryshop.canOperate
    ],
    controller.getBooking
  );
};