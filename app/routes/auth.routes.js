const { authJwt, verification, registration, authOtp } = require("../middleware");
const controller = require("../controllers/auth.controller");

module.exports = function(app) {

  app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
    next();
  });

  app.post("/api/auth/device", controller.authenticateDevice);

  app.post(
    "/api/auth/signup",
    [
      authJwt.verifyDeviceToken,
      verification.checkUserSession,
      verification.checkUserDetails,
      authOtp.generateOtp,
      authOtp.generateNotification
    ],
    controller.signup
  );

  app.post("/api/auth/signin", controller.signin);

  app.post("/api/auth/verify", controller.verifyOtp);

  // Not used by customer app
  app.post("/api/auth/profile", controller.updateProfile);

  // Not used by customer app
  app.get(
    "/api/auth/profile/:id",
    [authJwt.verifyToken],
    controller.profile
  );

  app.post("/api/auth/email/otp", controller.sendEmailOtp);

  app.put("/api/auth/first-reset-password", [
    authJwt.verifyToken
  ], controller.firstResetPassword);

  app.get("/api/auth/refresh-login", [
    authJwt.verifyToken
  ], controller.refreshLogin);
};