const { authJwt } = require("../middleware");
const controller = require("../controllers/user.controller");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
    next();
  });

  app.get("/api/test/all", controller.allAccess);

  app.get(
    "/api/test/user",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.userBoard
  );

  app.get(
    "/api/test/admin",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.adminBoard
  );

  app.get(
    "/api/test/laundry",
    [authJwt.verifyToken, authJwt.isLaundrySupport, authJwt.isAdmin],
    controller.laundryOwnerBoard
  );

  app.get(
    "/api/test/drivers",
    [authJwt.verifyToken, authJwt.isDriverSupport, authJwt.isAdmin],
    controller.driverBoard
  );

  app.get(
    "/api/customers",
    [authJwt.verifyToken, authJwt.isCustomerSupport, authJwt.isAdmin],
    controller.customerBoard
  );
};