
const db = require("../models");

module.exports = (laundryshops) => {
    const isArray = Array.isArray(laundryshops);
    laundryshops = isArray ? laundryshops : [laundryshops];
    
    const results = laundryshops.map((shop) => {
        const perKiloId = db.LOAD_TYPES.find((value) => value.name === 'per_kilo').id;
        const perLoadId = db.LOAD_TYPES.find((value) => value.name === 'per_load').id;
        const regularId = db.SERVICE_TYPES.find((value) => value.name === 'regular').id;
        const rushId = db.SERVICE_TYPES.find((value) => value.name === 'rush').id;

        shop = shop.dataValues;
        settings = shop.store_setting.dataValues;

        const perKilo = {
            regular: settings.store_services.find(value => value.dataValues.load_type_id === perKiloId && value.dataValues.service_type_id === regularId),
            rush: settings.store_services.find(value => value.dataValues.load_type_id === perKiloId && value.dataValues.service_type_id === rushId),
        };

        const perLoad = {
            regular: settings.store_services.find(value => value.dataValues.load_type_id === perLoadId && value.dataValues.service_type_id === regularId),
            rush: settings.store_services.find(value => value.dataValues.load_type_id === perLoadId && value.dataValues.service_type_id === rushId),
        };

        return {
            id: shop.id,
            name: shop.name,
            more_details: shop.more_details,
            remarks: shop.remarks,
            is_online: shop.is_online,
            first_setup: shop.first_setup,
            store_setting: {
                id: settings.id,
                open_hours: settings.open_hours,
                closing_hours: settings.closing_hours,
                logo: settings.id,
                background: settings.id,
                perLoad,
                perKilo
            }
        }
    });

    return isArray ? results : results[0];
};