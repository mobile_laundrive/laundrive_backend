const envConfig = require('dotenv').config();

if (envConfig.parsed?.TESTING) {
  const defaultConfig = require('./db.config.test.js');
  module.exports = defaultConfig;
  return;
}

module.exports = {
  HOST: "localhost",
  USER: "mzrvsjknwr",
  PASSWORD: "SRxdGKZf52",
  DB: "mzrvsjknwr",
  dialect: "mysql",
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
};