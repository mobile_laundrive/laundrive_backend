const db = require("../models");
const { generateOTP } = require("../utils/otp.util");

const Roles = db.Roles;
const User = db.user;
const Otp = db.otp;
const UserLoginSession = db.userLoginSession

saveLoginSession = (req, res, next) => {
    var createArgs = {};
    var whereArgs = {};

    var email = req.body.email;
    var phone = req.body.phone;

    if (email) {
        createArgs.email = email;
        whereArgs.email = email;
    }

    if (phone) {
        createArgs.phone = phone;
        whereArgs.phone = phone;
    }

    UserLoginSession
        .findOne({where: whereArgs})
        .then(session => {

            if (session) {
                createOtpCode(session, req, res, next);
                return;
            }

            UserLoginSession
                .create(createArgs)
                .then(session => createOtpCode(session, req, res, next))

        })
        .catch(err => res.send({ message: err.message }));

};

saveUser = (req, res, next) => {

var roleId = 5;

var createArgs = {};
var whereArgs = {};

var email = req.body.email;
var phone = req.body.phone;

if (email) {
    createArgs.email = email;
    whereArgs.email = email;
}

if (phone) {
    createArgs.phone = phone;
    whereArgs.phone = phone;
}

createArgs.roleId = roleId;

User.findOne({where: whereArgs})
    .then(existingUser => {
        
        if (existingUser) {
            createOtpCode(existingUser, req, res, next);   
            return;
        }

        // Create user
        User
        .create(createArgs)
        .then(newUser => createOtpCode(newUser, req, res, next))
        .catch(err => res.send({ message: err.message }));
    })
    .catch(err => res.send({ message: err.message }));
};

createOtpCode = (userLoginSession, req, res, next) => {
    const otpCode = generateOTP(6);
    
    var includeArgs = {
        include: [db.associations.OTP_USER]
    };

    // Delete the previous OTP Code
    var otpCodeId = userLoginSession.otpCodeId;
    if (otpCodeId) {
        Otp.destroy({
            where: { id: otpCodeId }
        });
    }

    var otpCreateArgs = {
        code: otpCode,
        expiry: new Date(Date.now() + (5 * 60 * 1000)),
    };
    
    // Create the OTP model and update if user exists
    Otp
    .create(otpCreateArgs)
    .then(async otp => {
        await userLoginSession.update({otpCodeId: otp.id});
        req.body.session = userLoginSession;
        req.body.otp = otp.code;
        next();
    })
    .catch(err => res.send({ message: err.message, session: userLoginSession }));
}

const registration = {
    startUserLoginSession: saveLoginSession,
    saveUser: saveUser,
};

module.exports = registration;