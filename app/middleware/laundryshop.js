
const { laundryShopUser } = require("../models");

canOperate = (req, res, next) => {
    laundryShopUser.findOne({
        where: {
            laundry_shop_id: +req.params.laundryshopId,
            user_id: req.userId
        }
    }).then(shop => {
        if(shop) {
            next();
            return;
        }

        res.status(403).send({
            message: "Shop cannot update this booking status!"
        });
    });
};
const laundryshop = {
    canOperate
};

module.exports = laundryshop;