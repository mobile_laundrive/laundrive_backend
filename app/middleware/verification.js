const e = require("cors");
const { response } = require("express");
const nodemailer = require('nodemailer');
const db = require("../models");

const Roles = db.Roles;
const User = db.user;
const Otp = db.otp;
const UserLoginSession = db.userLoginSession;

const EMAIL_ADDRESS = 'noreply@api.laundrive.xyz';
const { subject_mail, message } = require('../templates/email/verification');

const emailTransporter = nodemailer.createTransport({
  host: 'smtp.elasticemail.com',
  port: 2525,
  secureConnection: false,
  auth: {
    user: '317B24C81E97CFF6E65BFF5B07286CD8E41C3D49DDDB39FE66797562670B97750703A7B34C29EA9CF80FE62994E4B485',
    pass: '317B24C81E97CFF6E65BFF5B07286CD8E41C3D49DDDB39FE66797562670B97750703A7B34C29EA9CF80FE62994E4B485'
  },
  tls: {
    ciphers: 'SSLv3'
  }
});

// Uncomment if needed
checkDuplicateUsernameOrEmail = (req, res, next) => {

  /*var whereArg = { verified: true };
  var response = { error: true };
  var email = req.body.email;
  var phone = req.body.phone;

  if (email) {
    whereArg.email = email;
    response.message = "Email used.";
  }

  if (phone) {
    whereArg.phone = phone;
    response.message = "Phone number used.";
  }

  if (Object.keys(whereArg).length != 0) {

    User.findOne({
      where: whereArg
    }).then(user => {

      if (user && user.verified)
        req.verified = true;

      next();
    });
  } */
};

checkUserSession = (req, res, next) => {
  var createArgs = {};
  var whereArgs = {};

  let email = req.body.email;
  let phone = req.body.phone;

  if (email) {
      createArgs.email = email;
      whereArgs.email = email;
  }

  if (phone) {
      createArgs.phone = phone;
      whereArgs.phone = phone;
  }

  UserLoginSession
    .findOne(whereArgs)
    .then(async session => {

      // Destroy item if expired
      if (session) {
        let expired = Date.now() > session.expiry.getTime();
        if (expired)
          await session.destroy();
      }

      next();
    })
};

checkUserDetails = (req, res, next) => {
  var createArgs = {};
  var whereArgs = {};

  var email = req.body.email;
  var phone = req.body.phone;

  if (email) {
      createArgs.email = email;
      whereArgs.email = email;
  }

  if (phone) {
      createArgs.phone = phone;
      whereArgs.phone = phone;
  }

  if (Object.keys(whereArgs).length != 0) {

    User.findOne({
      where: whereArgs
    }).then(user => {
        
      req.hasUserDetails = null != user && undefined != user;

      if (user)
        req.userDetailsId = user.id;
      
      next();
    });
  }
};

checkRolesExisted = (req, res, next) => {
  if (req.body.role) {
    console.log('checking... roles');
    const found = Roles.find(el => el.id === req.body.role);
    if (!found) {
      res.status(400).send({
        message: "Failed! Role does not exist = " + req.body.role
      });
      return;
    }
  }

  next();
};

const verification = {
  checkUserSession: checkUserSession,
  checkUserDetails: checkUserDetails,
  checkDuplicateUsernameOrEmail: checkDuplicateUsernameOrEmail,
  checkRolesExisted: checkRolesExisted,
};

module.exports = verification;