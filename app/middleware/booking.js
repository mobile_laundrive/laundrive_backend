
const db = require("../models");
const Booking = db.booking;

isBookedToThisShop = (req, res, next) => {
    Booking.findByPk(+req.params.bookingId).then(booking => {
      if (booking.dataValues && booking.dataValues.laundry_shop_id === +req.params.laundryshopId) {
        next();
        return;
      }
      
      res.status(403).send({
        message: "Shop cannot update this booking status!"
      });
  
      return;
    });
};
const booking = {
    isBookedToThisShop
};

module.exports = booking;