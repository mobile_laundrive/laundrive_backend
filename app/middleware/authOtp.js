const e = require("cors");
const nodemailer = require('nodemailer');
const db = require("../models");
const { response } = require("express");
const { generateOTP } = require("../utils/otp.util");

const Roles = db.Roles;
const User = db.user;
const Otp = db.otp;

const EMAIL_ADDRESS = 'noreply@api.laundrive.xyz';
const { subject_mail, message } = require('../templates/email/verification');

const emailTransporter = nodemailer.createTransport({
  host: 'smtp.elasticemail.com',
  port: 2525,
  secureConnection: false,
  auth: {
    user: '317B24C81E97CFF6E65BFF5B07286CD8E41C3D49DDDB39FE66797562670B97750703A7B34C29EA9CF80FE62994E4B485',
    pass: '317B24C81E97CFF6E65BFF5B07286CD8E41C3D49DDDB39FE66797562670B97750703A7B34C29EA9CF80FE62994E4B485'
  },
  tls: {
    ciphers: 'SSLv3'
  }
});

generateUserCode = (req, res, next) => {
  req.body.otp = {
    code: generateOTP(6),
    expiry: new Date(Date.now() + (1 * 60 * 1000))
  };
  next();
};

destroyOtp = (req, res, next) => {
  var otpCodeId = -1;
  if (otpCodeId) {
      Otp.destroy({
          where: { id: otpCodeId }
      });
  }
};

generateOtpNotification = (req, _res, next) => {
  
    var email = req.body.email;
    var phone = req.body.phone;
    var otp = req.body.otp;
  
    if (null == otp || undefined == otp)
      return;
    
    let code = otp.code;
  
    if (email) {
      const mailOptions = {
        from: `"Laundrive Corporation"<${EMAIL_ADDRESS}>`,
        to: `${email}`,
        subject: "Your One-Time Pin code",
        text: message(code),
      };
  
      emailTransporter.verify();
      emailTransporter.sendMail(mailOptions);
    }
  
    if (phone) {
      // TODO
    }
  
    next();
};

const otpModule = {
    generateOtp: generateUserCode,
    generateNotification: generateOtpNotification
};
module.exports = otpModule