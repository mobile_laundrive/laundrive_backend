const authJwt = require("./authJwt");
const verification = require("./verification");
const registration = require("./registration");
const authOtp = require("./authOtp");
const booking = require("./booking");


module.exports = {
  authJwt,
  authOtp,
  verification,
  registration,
  booking
};