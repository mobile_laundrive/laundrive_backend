const jwt = require("jsonwebtoken");
const config = require("../config/auth.config.js");
const db = require("../models");
const User = db.user;
const ROLES = db.Roles;
const Device = db.device

verifyToken = (req, res, next) => {
  let authHeader  = req.headers.authorization;
  const token = authHeader?.split(' ')[1];
 
  if (!token) {
    return res.status(403).send({
      message: "No token provided!"
    });
  }

  jwt.verify(token, config.secret, async (err, decoded) => {
    
    if (err) {
      return res.status(401).send({data:{
        message: "Unauthorized!"
      }});
    }

    var deviceName = decoded.deviceName;
    if (deviceName) {
      req.deviceName = decoded.deviceName;

      var device = await Device.findOne({name: decoded.deviceName});

      if (device) {
        next();
      } else {
        res.status(404).send({ data: { error: true, message: "Device not found" } });
      }

      return;
    }
    
    req.userId = decoded.id;
    next();
  });
};

verifyDeviceToken = (req, res, next) => {
  let authHeader  = req.headers.authorization;
  const token = authHeader?.split(' ')[1];

  let errorMessage = "Unauthorized!"
 
  if (!token) {
    return res.status(401).send({
     data: {error: true, message: errorMessage}
    });
  }

  jwt.verify(token, config.secret, async (err, decoded) => {
    
    if (err) {
      return res.status(401).send({
        data: {
          error: true,
          message: errorMessage
        }
      });
    }
    next();
  });
};

isAdmin = (req, res, next) => {
  User.findByPk(req.userId).then(user => {
    const found = ROLES.find(el => el.id === user.roleId);
      if (found.name === "admin") {
        next();
        return;
      }

      res.status(403).send({
        message: "Require Admin Role!"
      });
      return;
  });
};

isCustomerSupport = (req, res, next) => {
  User.findByPk(req.userId).then(user => {
    const found = ROLES.find(el => el.id === user.roleId);
    if (found.name === "admin" || found.name === "customer_support") {
      next();
      return;
    }

    res.status(403).send({
      message: "Require Admin and Customer Role!"
    });
    return;
  });
};

isLaundrySupport = (req, res, next) => {
  User.findByPk(req.userId).then(user => {
    const found = ROLES.find(el => el.id === user.roleId);
    if (found.name === "admin" || found.name === "laundry_support") {
      next();
      return;
    }

    res.status(403).send({
      message: "Require Admin and Laundry Role!"
    });
    return;
  });
};

isDriverSupport = (req, res, next) => {
  User.findByPk(req.userId).then(user => {
    const found = ROLES.find(el => el.id === user.roleId);
    if (found.name === "admin" || found.name === "driver_support") {
      next();
      return;
    }

    res.status(403).send({
      message: "Require Admin and Driver Support Role!"
    });
    return;
  });
};

isLaundryShopOperator = (req, res, next) => {
  User.findByPk(req.userId).then(user => {
    const found = ROLES.find(el => el.id === user.roleId);
    if (found.name === "laundryshop_operator") {
      next();
      return;
    }

    res.status(403).send({
      message: "Laundryshop Operator Role!"
    });

    return;
  });
};

const authJwt = {
  verifyToken,
  verifyDeviceToken,
  isAdmin: isAdmin,
  isCustomerSupport,
  isLaundrySupport,
  isDriverSupport,
  isLaundryShopOperator
};
module.exports = authJwt;