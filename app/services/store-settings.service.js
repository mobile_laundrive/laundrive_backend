const db = require("../models");

const update = async (settingsId, data) => {
  return db.storeSettings.update(data, {
    where: {
      id: settingsId
    }
  });
}


const storeSettingsService = {
  update,
}

module.exports = storeSettingsService;
