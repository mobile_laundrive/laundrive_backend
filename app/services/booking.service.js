const { Sequelize } = require("../models");
const db = require("../models");
const calculation = require("./calculations");
const Booking = db.booking;
const BookingHistory = db.bookingHistory;
const StoreSetting = db.storeSettings;
const StoreService = db.storeService;

const bookingStatus = (status) => {
  const res = {
    cancelled: 1,
    waiting: 2,
    onTheWay: 3,
    pickup: 4,
    washing: 5,
    onTheWayToDeliver: 6,
    delivered: 7,
  }
  return res[status]
}
const Op = db.Sequelize.Op;

const createBooking = async (params) => {
    const {
      user_id,
      driver_id,
      laundry_shop_id,
      booking_statuses_id,
      ...items } = params;

     //create booking
    const booking = await Booking.create({
      user_id: user_id,
      driver_id: driver_id,
      laundry_shop_id: laundry_shop_id,
      booking_statuses_id: 1,
      items: items
    }, {
      include: [
        {
          association: Booking.associations.items
        }
      ]
    });
    
    // create booking history
    await BookingHistory.create({
      remarks: 'created booking',
      user_id: user_id,
      booking_id: booking.id,
    })

    // return all related models
    const bookingWithRelations = await Booking.findOne({ 
      where: {
        id: booking.id,
      },
      include: [{ all: true }]
    });
    return bookingWithRelations;
}

const updateStatus = async (id, statusId) => {
  return Booking.update({booking_statuses_id: statusId}, {
    where: {id}
  });
}

const updateBooking = async (params) => {
  const {
    id,
    status,
    ...items } = params;
    
    let amount = 0;
    let originalPrice = 0;
    const filter = {
      where: {
        id: parseInt(id),
      },
      include: [
       { association: Booking.associations.items},
       { association: Booking.associations.laundryShop}
      ]
    };

    const booking = await Booking.findOne(filter).then( async (record) => {
        if (record) {
          if (status === bookingStatus('washing')) {
            // do calculations
            const laundryShopId = record.laundryShop.id;
            const storeService = await StoreSetting.findOne({where: {laundry_shop_id: laundryShopId}})
              .then((setting) => {
                return StoreService.findOne({where: {store_settings_id: setting.id}});
              })
           
            if (storeService.load_type_id === 1) {
              originalPrice = parseInt(storeService.price);
              amount = await calculation.totalBookingAmount(parseInt(items.kilos), originalPrice);
            } else {
              amount = parseInt(items.load) * parseInt(storeService.price);
            }

            record.update({ booking_statuses_id: status, amount })
          } else {
            record.update({ booking_statuses_id: status})
          }

          record.items.update(items);
          return record;
        } else {
          return null;
        }
      });

    return booking;
}

// const getAllByShopIdWithSearchTerm = async (shopId, query = null) => {
const getAllByShopIdWithSearchTerm = async (shopId, statusIds, query = null) => {
  
  const whereClause = {
    laundry_shop_id: shopId,
  };

  if (statusIds) {
    const value = Array.isArray(statusIds) 
      ? {[Op.in]: statusIds}
      : statusIds;

    whereClause['booking_statuses_id'] = value
  }

  if(query) {
    whereClause[Op.and] = [
      {
        [Op.or]: [
          {id: {[Op.like]: `${query}%`}},
          Sequelize.where(Sequelize.fn('concat', Sequelize.col('user.first_name'), ' ', Sequelize.col('user.last_name')), {
            [Op.like]: `${query}%`
          }),
          Sequelize.where(Sequelize.fn('concat', Sequelize.col('user.last_name'), ' ', Sequelize.col('user.first_name')), {
            [Op.like]: `${query}%`
          })
        ]
      }
    ]
  }

  return Booking.findAll({
    where: whereClause,
    include: [
      {
        model: db.user,
        as: "user",
        attributes: {exclude: ['password', 'verified', 'verified', 'roleId', 'otpCodeId'],}
      },
      {
        model: db.user,
        as: "driver",
        attributes: {exclude: ['password', 'verified', 'verified', 'roleId', 'otpCodeId'],}
      },
      {
        model: db.laundryShop,
        required: true,
      },
      {
        model: db.bookingStatus,
        required: true,
        attributes: ['id', 'name']
      },
      {
        model: db.bookingStatus,
        required: true,
        attributes: ['id', 'name']
      },
      {
        model: db.item,
        as: 'items',
        include: [
          {
            model: db.storeService,
            as: 'store_services',
            include: [
              {
                model: db.serviceType
              }
            ]
          }
        ],
      }
    ]
  });
};

const getBooking = (id) => {
  return Booking.findOne({
    where: {id},
    include: [
      {
        model: db.user,
        as: "user",
        attributes: {exclude: ['password', 'verified', 'verified', 'roleId', 'otpCodeId'],}
      },
      {
        model: db.user,
        as: "driver",
        attributes: {exclude: ['password', 'verified', 'verified', 'roleId', 'otpCodeId'],}
      },
      {
        model: db.laundryShop,
        required: true,
      },
      {
        model: db.bookingStatus,
        required: true,
        attributes: ['id', 'name']
      },
      {
        model: db.item,
        as: 'items',
        include: [
          {
            model: db.storeService,
            as: 'store_services',
            include: [
              {
                model: db.serviceType
              }
            ]
          }
        ],
      }
    ]
  });
}

const bookingService = {
  createBooking,
  updateBooking,
  updateStatus,
  getAllByShopIdWithSearchTerm,
  getBooking
}
module.exports = bookingService;