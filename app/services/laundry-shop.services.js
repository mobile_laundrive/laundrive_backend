const { laundryShop } = require("../models");
const db = require("../models");
const LaundryShop = db.laundryShop;

const getLaundryShops = async (req, res) => {
  return LaundryShop.findAll({
    include:[
      {
        model: db.storeSettings,
        include: [{
          model: db.storeService,
          include: [
            {model: db.loadType, required: true}, 
            {model: db.serviceType, required: true}
          ]
        }]
      }
    ]
  });
};

const getLaundryShop = async (laundryShopId, res) => {
  const laundryShop = await LaundryShop.findOne({ where: { id: parseInt(laundryShopId) } });
  if (laundryShop === null) {
    return res.status(500).send({data:{ message: "Laundry Shop Not found!" }});
    
  }

  return laundryShop;
}

const getAllByUser = (userId, res) => {
  return LaundryShop.findAll({
    include: [{
      model: db.laundryShopUser,
      required: true,
      where: {
        user_id: userId
      }
    },{
      model: db.storeSettings,
      include: [
        {
          model: db.storeService,
          include: [
            {
              model: db.loadType,
              required: true
            }, 
            {
              model: db.serviceType,
              required: true
            }
          ]
        }
      ]
    }]
  });
};

const getById = (shopId) => {
  return LaundryShop.findOne({
    where: {
      id: shopId
    },
    include: [{
      model: db.laundryShopUser,
      required: true,
    },{
      model: db.storeSettings,
      include: [
        {
          model: db.storeService,
          include: [
            {
              model: db.loadType,
              required: true
            }, 
            {
              model: db.serviceType,
              required: true
            }
          ]
        }
      ]
    }]
  });
};

const updateStatus = (id, isOnline) => {
  return laundryShop.update({is_online: isOnline}, {
    where: {id}
  });
}

const laundryShopService = {
  getLaundryShops,
  getLaundryShop,
  getAllByUser,
  updateStatus,
  getById
}
module.exports = laundryShopService;
