const db = require("../models");
const { Op } = require("sequelize");
const User = db.user;
const Driver = db.driver;

const getUser = async (userId, res) => {
  const user = await User.findOne({ where: { id: parseInt(userId) } });
  if (user === null) {
    return res.status(500).send({data:{ message: "User Not found!" }});
    
  }

  return user;
}

const getPagingData = (data, page, limit) => {
  const { count: totalItems, rows } = data;
  const currentPage = page ? +page : 0;
  const totalPages = Math.ceil(totalItems / limit);

  return { totalItems, rows, totalPages, currentPage };
};

const getPagination = (page, size) => {

  // If the page is not applied in query
  if (!page) {
    // Make the Default value one
    page = 1;
  }

  if (!size) {
    size = 10;
  }
    
  const limit = parseInt(size);
  const offset = (page > 1) ? (page * limit - limit) : 0;

  return { limit, offset };
};

const getCustomers = async (page, size, search) => {
  const { limit, offset } = getPagination(page, size);


  let args = {
    role_id: 5,
  }
  if (search) {
    args = {...args, first_name: {[Op.like]: `%${search}%`}}
  }

  const customers = await User.findAndCountAll({
    where: args, offset, limit,
  });

  if (customers === null) {
    return res.status(500).send({data:{ message: "No available customers!" }});
  }

  const response = getPagingData(customers, page, limit);

  return response;
}

const getDriver = async (driverId, res) => {
  const driver = await User.findOne({ where: { id: parseInt(driverId), role_id: 6 } });

  if (driver === null) {
    return res.status(500).send({data:{ message: "Driver Not found!" }});
  }

  return driver;
}

const userService = {
  getUser,
  getDriver,
  getCustomers,
}
module.exports = userService;
