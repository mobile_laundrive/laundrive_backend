const { operationCity } = require("../models")

const getOperationCities = async (req, res) => {
    return operationCity.findAll();
}

const operationCitiesService = {
    getOperationCities
}

module.exports = operationCitiesService;