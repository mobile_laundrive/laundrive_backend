const variables = require("../constants/variables");
const {
  LAUNDRIVE_MARKUP_PRICE,
  MINIMUM_KG,
  VALUE_ADDED_TAX,
  PICKUP_AND_DELIVERY_FEE
} = variables

const totalBookingAmount = (additionalKg = 0, originalPrice = 0) => {
  var pricePerKilo = (originalPrice * LAUNDRIVE_MARKUP_PRICE) + originalPrice;
  let updatedOriginalPrice = pricePerKilo * (MINIMUM_KG + additionalKg);
  let vat = updatedOriginalPrice * VALUE_ADDED_TAX;
  let updatedOriginalPriceWithVat = updatedOriginalPrice + vat;
  let total = updatedOriginalPriceWithVat + PICKUP_AND_DELIVERY_FEE;
  return total.toFixed(2);
}

const calculation = {
  totalBookingAmount,
}

module.exports = calculation;