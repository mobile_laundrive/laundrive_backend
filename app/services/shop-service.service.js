const db = require("../models");

const update = async (serviceId, data) => {
  return db.storeService.update({
    "max_daily_transactions": +data.max_daily_transactions,
    "min_kg": +data.min_kg,
    "max_kg": +data.max_kg,
    "price": +data.price,
    "delivery_time": +data.delivery_time,
    "is_active": data.is_active
  }, {
    where: {
      id: serviceId
    }
  });
}


const shopService = {
  update,
}

module.exports = shopService;
