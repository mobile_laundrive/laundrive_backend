const subject_mail = "OTP: For Reset Password"

const message = (otp) =>{
  return `Dear User, \n\n` 
  + 'Your One-Time Pin is: '
  + `${otp}\n\n`
  + 'This is a auto-generated email. Please do not reply to this email.\n\n'
  + 'Regards\n'
  + 'Laundrive Corporation\n\n'
}

module.exports = {subject_mail, message};